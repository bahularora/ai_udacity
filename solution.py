assignments = []


def assign_value(values, box, value):
    """
    Please use this function to update your values dictionary!
    Assigns a value to a given box. If it updates the board record it.
    """
    values[box] = value
    if len ( value ) == 1:
        assignments.append ( values.copy ( ) )
    return values


def naked_twins(values):
    """Eliminate values using the naked twins strategy.
    Args:
        values(dict): a dictionary of the form {'box_name': '123456789', ...}
    
    Returns:
        the values dictionary with the naked twins eliminated from peers.
    
    What I am trying to do here is to find all naked twins for each unit and then remove the digits from its peers.
    """

    # finding naked twins
    for unit in unitlist:
        # collecting all boxes of length 2
        all_two_len = []
        for box in unit:
            if len(values[box]) == 2:
                all_two_len.append(box)
        pairs = []
        count = len(all_two_len)
        # checking all pairs for naked pair
        for i in range(count):
            for j in range(i + 1, count):
                if values[ all_two_len[i] ] == values[ all_two_len[j] ]:  # if they match append them to pairs
                    pairs.append( [ all_two_len[i], all_two_len[j], values[all_two_len[i]] ] )

        # removing digits from naked twins found in the other units

        # iterating over each pair of naked twin found
        for each in pairs:
            # asigning the digits in naked twins to the translation table, to later remove from other boxes
            translation_table = dict.fromkeys ( map ( ord, each[2] ), None )
            for each_box in unit:  # iterating over the unit to remove the digits from naked twins
                # remove only if the box is not one of the twin itself
                if each_box != each[0] and each_box != each[1]:
                    # removing all the characters defined in translation_table i.e the char in naked twins
                    values[each_box] = values[each_box].translate ( translation_table )
    return values

    # Find all instances of naked twins
    # Eliminate the naked twins as possibilities for their peers


rows = 'ABCDEFGHI'
cols = '123456789'


def cross(A, B):
    return [s + t for s in A for t in B]


def diag(a, b):
    return [ [c + b[i] for i, c in enumerate ( a )] ]+ [[c + b[-i - 1] for i, c in enumerate ( a )]]


boxes = cross ( rows, cols )
row_units = [cross ( r, cols ) for r in rows]
column_units = [cross ( rows, c ) for c in cols]
square_units = [cross ( rs, cs ) for rs in ('ABC', 'DEF', 'GHI') for cs in ('123', '456', '789')]
diagonal_units = diag(rows,cols)
unitlist = row_units + column_units + square_units + diagonal_units
units = dict ( (s, [u for u in unitlist if s in u]) for s in boxes )
peers = dict ( (s, set ( sum ( units[s], [] ) ) - set ( [s] )) for s in boxes )


def grid_values(grid):
    """
    Convert grid into a dict of {square: char} with '123456789' for empties.
    Args:
        grid(string) - A grid in string form.
    Returns:
        A grid in dictionary form
            Keys: The boxes, e.g., 'A1'
            Values: The value in each box, e.g., '8'. If the box has no value, then the value will be '123456789'.
    """

    values = dict ( )
    all_no = '123456789'
    for i, c in enumerate ( grid ):  # iterating over chars in the the grid
        if c == '.':  # if the char is a '.' we set it to all_no
            values[boxes[i]] = all_no  # setting it to all digits
        else:  # else setting it to the char
            values[boxes[i]] = c
    return values


def display(values):
    """
    Display the values as a 2-D grid.
    Args:
        values(dict): The sudoku in dictionary form
    """
    width = 1 + max ( len ( values[s] ) for s in boxes )
    line = '+'.join ( ['-' * (width * 3)] * 3 )
    for r in rows:
        print ( ''.join ( values[r + c].center ( width ) + ('|' if c in '36' else '')
                          for c in cols ) )
        if r in 'CF': print ( line )
    return


def eliminate(values):
    for key, value in values.items():
        #print(key,value)
        if len(value) == 1:
            #print('value is len 1')
            translation_table = {ord(value) : None}
            for each_peer in peers[key]:
                #print('')
                values[each_peer] = values[each_peer].translate(translation_table)
    return values


def only_choice(values):
    for unit in unitlist:
        # Initialising a dict with each digit = 0 signaling each digit hasn't been found yet
        d = {'0': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, "6": 0, "7": 0, "8": 0,
             "9": 0}
        for box in unit:
            for each_digit in values[box]:
                # if the value is 0 then this is the first time we have encountered this digit in the unit
                if d[each_digit] == 0:
                    # as this is the first time we have encountered the digit,
                    # we assign it the box where this digit was encountered,
                    # we will also check later that this digit is not repeated again in any other box,
                    # in which case we mark it with X
                    d[each_digit] = box
                # if value is not zero it means the digit has been encounter before
                # and hence it can't be assigned to a box
                else:
                    # as it can't be assigned to a box we mark it with X
                    d[each_digit] = 'X'
        for key, value in d.items ( ):
            # if the value isn't X that means the digit was found once and can be safely assigned to the box
            if value != 0 and value != 'X':
                values[value] = key  # assigning digit to box.
    return values


def reduce_puzzle(values):
    stalled = False
    while not stalled:
        # Check how many boxes have a determined value
        solved_values_before = len ( [box for box in values.keys ( ) if len ( values[box] ) == 1] )
        # Your code here: Use the Eliminate Strategy

        values = eliminate(values)

        # Your code here: Use the Only Choice Strategy

        #display(values)
        values = only_choice(values)

        #display(values)
        # Using Naked twin
        values = naked_twins(values)
        #display(values)

        # Check how many boxes have a determined value, to compare
        solved_values_after = len( [box for box in values.keys() if len( values[box] ) == 1] )
        # If no new values were added, stop the loop.
        stalled = solved_values_before == solved_values_after
        # Sanity check, return False if there is a box with zero available values:
        if len ( [box for box in values.keys ( ) if len ( values[box] ) == 0] ):
            return False
    return values


def search(values):
    values = reduce_puzzle ( values )
    if values is False:
        return False  ## Failed earlier
    if all ( len ( values[s] ) == 1 for s in boxes ):
        return values  ## Solved!
    # Choose one of the unfilled squares with the fewest possibilities
    n, s = min ( (len ( values[s] ), s) for s in boxes if len ( values[s] ) > 1 )
    # Now use recurrence to solve each one of the resulting sudokus, and 
    for value in values[s]:
        new_sudoku = values.copy ( )
        new_sudoku[s] = value
        attempt = search ( new_sudoku )
        if attempt:
            return attempt


def solve(grid):
    """
    Find the solution to a Sudoku grid.
    Args:
        grid(string): a string representing a sudoku grid.
            Example: '2.............62....1....7...6..8...3...9...7...6..4...4....8....52.............3'
    Returns:
        The dictionary representation of the final sudoku grid. False if no solution exists.
    """

    values = grid_values ( grid )
    values = search ( values )
    #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$')
    #display(values)
    return values
    if __name__ == '__main__':
        diag_sudoku_grid = '2.............62....1....7...6..8...3...9...7...6..4...4....8....52.............3'
        display ( solve ( diag_sudoku_grid ) )

    try:
        from visualize import visualize_assignments

        visualize_assignments ( assignments )

    except SystemExit:
        pass
    except:
        print ( 'We could not visualize your board due to a pygame issue. Not a problem! It is not a requirement.' )
